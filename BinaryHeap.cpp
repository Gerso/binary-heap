/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BinaryHeap.cpp
 * Author: gerson
 * 
 * Created on 10 de Maio de 2018, 10:00
 */

#include "BinaryHeap.h"
#include "BinaryTree.h"
#include <iostream>
#include <cstdlib>

BinaryHeap::BinaryHeap(int M) {
    this->M = M;
    this->n = 0;
    this->heap = new No*[M];

    No *no = new No();
    no->setPriority(2147483647);
    heap[this->n] = no;
}

BinaryHeap::BinaryHeap(const BinaryHeap& orig) {
}

BinaryHeap::~BinaryHeap() {
}

void BinaryHeap::heapify(int array[], int size) {
    for (int i = 0; i < size; i++) {
        insert(array[i]);
    }
}

int BinaryHeap::size() {
    return this->n;
}

void BinaryHeap::insert(int value) {

    if (this->n < this->M) {

        this->n++;
        No *no = new No();
        no->setPriority(value);
        heap[this->n] = no;
        up(this->n);
        std::cout << "| Value inserted at position " << this->n << std::endl;
    } else {
        std::cout << "| The array is full.\n| Please increase capacity.";
    }
}

void BinaryHeap::update(int edit, int newPriority) {
    int i;

    if (this->search(edit)) {
        for (int c = 0; c <= this->n; c++) {
            if (this->heap[c]->getPriority() == edit) {
                i = c;
                this->heap[c]->setPriority(newPriority);
            }
        }

        if (edit < newPriority) {
            up(i);
        }
        if (edit > newPriority) {
            down(i, this->n);
        }
    } else {
        std::cout << "| The node not exist in the heap\n";
    }
}

void BinaryHeap::remove() {

    if (this->n != 0) {
        std::cout << "| Using the value " << this->heap[1] << std::endl;
        this->heap[1] = this->heap[this->n];
        this->n--;
        down(1, this->n);
    }
}

bool BinaryHeap::search(int value) {
    if (this->n != 0) {
        for (int i = 0; i <= this->n; i++) {
            if (this->heap[i]->getPriority() == value) {
                return true;
            }
        }
        return false;
    }
}

int BinaryHeap::select() {
    if (this->n != 0) {
        return this->heap[1]->getPriority();
    }
    return 0;
}

int BinaryHeap::heapCapacity() {
    return this->M;
}

void BinaryHeap::ChangeCapacity(int increment) {
    No** temp = NULL;

    if (increment > 0) {
        int aux = this->M + increment;
        temp = (No**) realloc(this->heap, aux * sizeof (No*));
        this->heap = temp;
        this->M = aux;

    } else {
        int decrement = (this->M) + (increment);

        if (decrement >= this->n) {

            temp = (No**) realloc(this->heap, decrement * sizeof (No*));
            this->heap = temp;
            this->M = decrement;
        } else {
            std::cout << "| impossible to perform operation\n"
                    "| because the capacity is\n"
                    "| equal to the size.\n";
        }
    }

}

void BinaryHeap::printHeap() {
    std::cout << "| [ ";
    for (int i = 1; i <= this->n; i++) {
        std::cout << this->heap[i]->getPriority() << ", ";
    }
    std::cout << "]";
}

void BinaryHeap::BinaryTreeToHeap(NoTree *no) {
    if (no != NULL) {
        this->insert(no->getKey());
        BinaryTreeToHeap(no->getLeft());
        BinaryTreeToHeap(no->getRight());
    }
}

void BinaryHeap::HeapToBinaryTree() {
    this->tree = new BinaryTree();

    NoTree *root = this->tree->getRoot();
    for (int i = 1; i <= this->n; i++) {
        root = this->tree->getRoot();
        this->tree->insert(root, this->heap[i]->getPriority());
    }
    root = this->tree->getRoot();
    this->tree->preOrder(root);
}

void BinaryHeap::up(int i) {
    int pai = i / 2;

    if (pai >= 1) {
        if (this->heap[i]->getPriority() > this->heap[pai]->getPriority()) {

            int aux = this->heap[i]->getPriority();
            this->heap[i]->setPriority(this->heap[pai]->getPriority());
            this->heap[pai]->setPriority(aux);

            up(pai);
        }
    }
}

void BinaryHeap::down(int i, int n) {
    int child = 2 * i;

    if (child <= n) {
        if (child < n) {
            if (this->heap[child + 1]->getPriority() > this->heap[child]->getPriority()) {
                child++;
            }
        }
        if (this->heap[i]->getPriority() < this->heap[child]->getPriority()) {
            int aux = this->heap[i]->getPriority();
            this->heap[i]->setPriority(this->heap[child]->getPriority());
            this->heap[child]->setPriority(aux);

            down(child, n);
        }
    }
}

BinaryTree* BinaryHeap::getTree() const {
    return this->tree;
}