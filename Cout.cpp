/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Cout.cpp
 * Author: gerson
 * 
 * Created on 11 de Maio de 2018, 10:06
 */

#include "Cout.h"
#include <iostream>

using namespace std;

Cout::Cout() {
}

Cout::Cout(const Cout& orig) {
}

Cout::~Cout() {
}

void Cout::head() {
    cout << "|------------------------------------|\n";
    cout << "|              BinaryHeap            |\n";
    cout << "|------------------------------------|\n";
}

void Cout::foot() {
    cout << "|------------------------------------|\n";
}

void Cout::menu() {
    cout << "|  1  - Heap Size                    |\n";
    cout << "|   2  - Insert                      |\n";
    cout << "|    3  - Update                     |\n";
    cout << "|     4  - Remove                    |\n";
    cout << "|      5  - Seach                    |\n";
    cout << "|       6  - Select                  |\n";
    cout << "|        7  - Heap capacity          |\n";
    cout << "|         8  - Change Capacity       |\n";
    cout << "|          9  - Print Heap           |\n";
    cout << "|           10 - Binary Tree To Heap |\n";
    cout << "|            11 - Heap To Binary Tree|\n";
    cout << "|             12 - Exit              |\n";
    cout << "| option = ";
}

