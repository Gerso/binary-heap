/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   NoTree.h
 * Author: gerson
 * class for the creation of the bianria tree
 */

#ifndef NOTREE_H
#define NOTREE_H

class NoTree {
public:


    NoTree();
    NoTree(const NoTree& orig);
    virtual ~NoTree();

    /* @param ket to set*/
    void setKey(int key);
    
    /* @return current key*/
    int getKey() const;
    
    /* @param set node right*/
    void setRight(NoTree* right);
    
    /* @return current node right*/
    NoTree* getRight() const;
    
    /* @param set node left*/
    void setLeft(NoTree* left);
    
    /* @return current node left*/
    NoTree* getLeft() const;

private:
    NoTree* left, *right;
    int key;
};

#endif /* NOTREE_H */

