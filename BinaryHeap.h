/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BinaryHeap.h
 * Author: gerson
 *
 * Created on 10 de Maio de 2018, 10:00
 */

#ifndef BINARYHEAP_H
#define BINARYHEAP_H

#include "No.h"
#include "BinaryTree.h"

class BinaryHeap {
public:
    /**
     * Constructor that instantiates my heap with the capacity passed by
     * parameter. Here also starts the array at position 0 with the largest
     * value supported by integer
     * @param M = capacity entered in main
     */
    BinaryHeap(int M);
    BinaryHeap(const BinaryHeap& orig);
    virtual ~BinaryHeap();

    /**
     * 
     * @param array = array of integers created in main
     * @param size = array size
     */
    void heapify(int array[],int size);
    
    /**
     * method that return he size of the Heap.  
     * @return the number of nodes inserted in the Heap
     */
    int size();

    /* Method to insert a heap node. When a new one is inserted the up
     * function is called to sort the heap.
     * @param value = integer value representing node priority
     */
    void insert(int value);

    /*
     * methos to update the priority of a node
     * @param edit = an existing current value
     * @param newPriority = new value to this no
     */
    void update(int edit, int newPriority);

    /*
     * Method to remove a heap node. When you remove one on it it calls the
     * function down to sort the heap
     */
    void remove();

    /**
     * Returns information (its key) from the highest priority node of the Heap
     * @return current root key  
     */
    int select();

    /**
     * Method to find out if a given priority exists in the heap
     * @param value = priority to be searched
     * @return true if exists or false if not exists 
     */
    bool search(int value);

    /**
     * Method to return the maximum number of nodes that a Stack can receive
     * @return Current M; 
     */
    int heapCapacity();

    /**
     * Method to changes the current capacity of the heap. the capacity of the heap can be
     * either increased or decreased. if the new desired capacity is less than 
     * the number of nodes present in the list, the capacity of the heap should
     * not be changed
     * @param increment = value of modification +1 .. to increment or -1 ... to decrease
     */
    void ChangeCapacity(int increment);

    /**
     * Method to print heap on screen
     */
    void printHeap();

    /**
     * Method to convert binary tree into binary heap
     * @param no = tree root
     */
    void BinaryTreeToHeap(NoTree *no);

    //
    /**
     * Method to converts a binaryheap into binary tree 
     */
    void HeapToBinaryTree();

    /**
     * Move up a node on the stack when it does not meet this order S1 <S1 / 2
     * @param i = current index 
     */
    void up(int i);

    /**
     * Down a node in the heap when it does not meet the order S1 <S1 / 2
     * @param i = current index
     * @param n = current size of the Heap
     */
    void down(int i, int n);

    /** @return  binary tree*/
    BinaryTree* getTree() const;

private:
    int M; // represents the capacity of the heap
    int n; // the number of values ​​in the heap
    No **heap; // array of nodes to represent heap
    BinaryTree *tree; // binary tree
};

#endif /* BINARYHEAP_H */