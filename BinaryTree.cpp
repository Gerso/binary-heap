/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BinaryTree.cpp
 * Author: gerson
 * 
 * Created on 14 de Maio de 2018, 17:32
 */

#include <stddef.h>
#include <iostream>
#include "BinaryTree.h"

BinaryTree::BinaryTree() {
}

int BinaryTree::search(NoTree*& no, int key) {
    int f = 0;

    if (no == NULL) {
        f = 0;
    } else if (key == no->getKey()) {
        f = 1;
    } else if (key < no->getKey()) {
        if (no->getLeft() == NULL) {
            f = 2;
        } else {
            no = no->getLeft();
            f = search(no, key);
        }
    } else {
        if (no->getRight() == NULL) {
            f = 3;
        } else {
            no = no->getRight();
            f = search(no, key);
        }
    }

    return f;
}

bool BinaryTree::insert(NoTree*& no, int key) {
    bool ok;
    int f;
    NoTree* pt;
    NoTree* aux;

    ok = true;
    aux = no;

    f = search(aux, key);
    if (f == 1) {
        ok = false;
    } else {
        pt = new NoTree();
        pt->setKey(key);
        pt->setLeft(NULL);
        pt->setRight(NULL);

        if (f == 0) {
            this->setRoot(pt);
        } else if (f == 2) {
            aux->setLeft(pt);
        } else {
            aux->setRight(pt);
        }
    }

    return ok;
}

void BinaryTree::preOrder(NoTree* no){
    
    if (no != NULL) {
      
        std::cout << no->getKey() << " ";
        preOrder(no->getLeft());
        preOrder(no->getRight());
    }
}

void BinaryTree::setRoot(NoTree* root) {
    this->root = root;
}

NoTree* BinaryTree::getRoot() const {
    return root;
}

BinaryTree::BinaryTree(const BinaryTree& orig) {
}

BinaryTree::~BinaryTree() {
}

