/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: gerson
 *
 * Created on 10 de Maio de 2018, 09:59
 */

#include <cstdlib>
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include "BinaryHeap.h"
#include "No.h"
#include "Cout.h"

using namespace std;

/*
 * 
 */

int main() {

    int option, insert, increment, select, search, edit, newPriority;
    bool send = true;
    int *array, size = 10;
    Cout *couts = new Cout();
    BinaryHeap* heap;

    /**object of type binary heap that will be used in the conversion of
     *binary tree to binary heap
     */
    BinaryHeap* treeToHeap;

    heap = new BinaryHeap(10);

    /**
     * automatic insert not to waste time
     */
    array = new int[size];

    array[0] = 95;
    array[1] = 73;
    array[2] = 78;
    array[3] = 33;
    array[4] = 60;
    array[5] = 66;
    array[6] = 70;
    array[7] = 20;
    array[8] = 25;
    array[9] = 28;
    heap->heapify(array, size);
    system("clear");

    /** 
     *The while send makes the program execution loop as long as you want
     */
    while (send) {
        system("clear");
        couts->head();
        couts->menu();

        cin>> option;

        couts->foot();

        switch (option) {
            case 1:// Case 1 shows the current size of the heap
                cout << "\t\tHeap Size\n";
                couts->foot();

                cout << "| Size of heap is = " << heap->size();

                break;
            case 2:// Case 2 calls the method to insert a value in the heap
                cout << "\t\tInsert\n";
                couts->foot();

                cout << "| Enter the value to be entered :";
                cin >> insert;

                heap->insert(insert);

                break;
            case 3:// Case 3 calls the method to autallize the priority of a heap node
                cout << "\t\tUpdate\n";
                couts->foot();

                cout << "| Enter current priority: ";
                cin >> edit;
                cout << "| Enter the value of the new priority: ";
                cin >> newPriority;

                heap->update(edit, newPriority);

                break;
            case 4:// Case 4 calls the method to remove a node in the heap
                cout << "\t\tRemove\n";
                couts->foot();

                heap->remove();

                break;
            case 5:// Case 5 calls the method to search for a node in the heap
                cout << "\t\tSeach\n";
                couts->foot();

                cout << "| Enter the priority to be searched: ";
                cin >> search;

                if (heap->search(search)) {
                    cout << "| The priority is in the heap\n";
                } else {
                    cout << "| The priority is not this heap\n";
                }

                break;
            case 6:// Case 6 calls the method that selects the highest priority node
                cout << "\t\tSelect\n";
                couts->foot();

                select = heap->select();
                if (select != 0) {
                    cout << "| The highest priority node is: " << select << endl;
                } else {
                    cout << "| The heap is null\n";
                }

                break;
            case 7:// Case 7 shows the capacity of the heap
                cout << "\t  Heap capacity\n";
                couts->foot();

                cout << "| The heap capacity is : " << heap->heapCapacity() << endl;

                break;
            case 8:// Case 8 calls the method to change the capacity of the heap
                cout << "\t  Change Capacity\n";
                couts->foot();

                cout << "| Enter the increment value: ";
                cin >> increment;
                heap->ChangeCapacity(increment);

                break;
            case 9:// Case 9 print the heap
                cout << "\t     Print Heap\n";
                couts->foot();

                heap->printHeap();

                break;
            case 10:// Case 10 convert a binary tree into a heap binary
                cout << "\t  Binary Tree To Heap\n";
                couts->foot();

                treeToHeap = new BinaryHeap(heap->heapCapacity());
                treeToHeap->BinaryTreeToHeap(heap->getTree()->getRoot());
                treeToHeap->printHeap();
                break;
            case 11://Case 11 to convert a binary heap into a binary tree
                cout << "\t  Heap To Binary Tree\n";
                couts->foot();

                heap->HeapToBinaryTree();

                break;
            case 12:// Case 12 puts the send variable in false and quits the program
                cout << "\t\t   Exit\n";
                couts->foot();

                send = false;

                break;
            default://....
                cout << "\t\t|  choose a valid value\n";
                couts->foot();
        }
        char key;
        cout << "\n\tpress any key!!\n";
        cin >> key;
    }

    return 0;
}

