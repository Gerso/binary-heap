/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BinaryTree.h
 * Author: gerson
 *
 * Created on 14 de Maio de 2018, 17:32
 */

#ifndef BINARYTREE_H
#define BINARYTREE_H

#include "NoTree.h"


class BinaryTree {
public:
    BinaryTree();
    BinaryTree(const BinaryTree& orig);
    virtual ~BinaryTree();
    
    /*
     * Method to check if the value already exists or where it will be inserted,
     * whether it is on the right or on the left
     * @param no = address of the arvover root
     * @param key = key to be searched
     * @retrun 1 if exists, 2 if it is to insert in the left, 
     * 3 if it is to insert in the right, 0 if root is null 
     */
    int search(NoTree *&no, int key);
    
    /*
     * Method to insert a new node in the tree, this method calls the search
     * function to know where to insert
     * @param no = address of the arvover root
     * @param key = key to be inserted
     * @return true if insert and false if not insert
     */
    bool insert(NoTree *&no, int key);
    
    /**
     * method to run the tree in preorder, where the node is visited before 
     * @param no = current node
     */
    void preOrder(NoTree * no);
    
    /** @param root to set*/
    void setRoot(NoTree* root);
    
    /**@return current root*/
    NoTree* getRoot() const;
    
private:
    
    NoTree* root;
};

#endif /* BINARYTREE_H */

