/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   NoTree.cpp
 * Author: gerson
 * 
 * Created on 14 de Maio de 2018, 17:21
 */

#include "NoTree.h"

NoTree::NoTree() {

}

NoTree::NoTree(const NoTree& orig) {
}

NoTree::~NoTree() {
}

void NoTree::setKey(int key) {
    this->key = key;
}

int NoTree::getKey() const {
    return key;
}

void NoTree::setRight(NoTree *right) {
    this->right = right;
}

NoTree* NoTree::getRight() const {
    return this->right;
}

void NoTree::setLeft(NoTree* left) {
    this->left = left;
}

NoTree* NoTree::getLeft() const {
    return this->left;
}

