/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Cout.h
 * Author: gerson
 * Class to mount the menu that will be shown on the screen
 * Created on 11 de Maio de 2018, 10:06
 */

#ifndef COUT_H
#define COUT_H

class Cout {
public:
    Cout();
    Cout(const Cout& orig);
    virtual ~Cout();
    
    /* shows a line with the header */
    void head();

    /*print on the screen one line at the end of the menu*/
    void foot();

    /*print on the screen a menu with all the options*/
    void menu();
private:

};

#endif /* COUT_H */

