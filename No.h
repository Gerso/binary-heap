/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   No.h
 * Author: gerson
 * Class for binaryheap creation
 * Created on 10 de Maio de 2018, 10:32
 */

#ifndef NO_H
#define NO_H

class No {
public:
    No();
    virtual ~No();
    
    /* @param priority to set*/
    void setPriority(int priority);
    
    /* @return current priority*/
    int getPriority() const;
    
    
private:
    int priority;
};

#endif /* NO_H */

